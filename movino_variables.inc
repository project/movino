<?php // $Id$
/*
    Movino Web Frontend - Server handling, feed parsing and feed caching
    Copyright 2006, 2007 Tom Sundström

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/


/**
 * Metadata fields available for movino content.
 */
function movino_metadata() {
  return array(
    'player' => t('Embedded player'),
    'icon' => t('Preview icon'),
    'title' => t('Title'),
    'author' => t('Author'),
    'created' => t('Created'),
    'length' => t('Duration'),
    'url' => t('Direct link'),
    'encoding' => t('Encoding'),
  );
}


/**
 * Tell which metadata fields are enabled by default.
 */
function movino_metadata_enabled($teaser = FALSE) {
  if ($teaser) {
    return variable_get('movino_metadata_teaser', array('icon', 'title', 'author', 'created', 'length'));
  }

  return variable_get('movino_metadata_page', array('player', 'title', 'author', 'created', 'length', 'url'));
}


/**
 * Tell if metadata labels should be hidden.
 *
 * @param boolean $teaser
 * @return array
 */
function movino_metadata_labels_enabled($teaser = FALSE) {
  if ($teaser) {
    return variable_get('movino_metadata_labels_enabled_teaser', array());
  }

  return variable_get('movino_metadata_labels_enabled_page', array('title', 'author', 'created', 'length', 'url'));
}


/**
 * Whether or not to allow the embedded player to be resized.
 */
function movino_player_embed_allow_resize($video) {
  if (isset($video['allow_resize'])) {
    return $video['allow_resize'];
  }
  return variable_get('movino_player_embed_allow_resize', 1);
}


/**
 * Maximum size of embedded video.
 */
function movino_player_embed_scale_max() {
  return 4;
}


/**
 * Minimum size of embedded video.
 */
function movino_player_embed_scale_min() {
  return 1;
}
