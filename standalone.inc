<?php // $Id$
/*
    Movino-web
    Copyright 2006, 2007 Tom Sundström

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/

define('MOVINO_STANDALONE', TRUE);
include_once('movino.module');
include_once('optional/movino_lists.module');

if (is_file('../movino_player_cortado/movino_player_cortado.module')) {
  include_once('../movino_player_cortado/movino_player_cortado.module');
  define('PATH_CORTADO', '../movino_player_cortado');
} elseif (is_file('./movino_player_cortado.module')) {
  include_once('./movino_player_cortado.module');
  define('PATH_CORTADO', '.');
} elseif (is_file('./movino_player_cortado/movino_player_cortado.module')) {
  include_once('./movino_player_cortado/movino_player_cortado.module');  
  define('PATH_CORTADO', './movino_player_cortado');
}

class movinoController {
  
  function movinoController() {
    if (!isset($_REQUEST['q'])) {
      $_REQUEST['q'] = 'movino';
    }
  }
  
  function page() {
    
    // Fetch info on all pages the movino module and the movino list module implements.
    $paths = array_merge(movino_menu(0), movino_menu(1));
    $paths = array_merge($paths, movino_lists_menu(0));
    $paths = array_merge($paths, movino_lists_menu(1));
    
    foreach ($paths as $path) {
      if ($path['path'] == substr($_REQUEST['q'], 0, strlen($path['path'])) &&
          ereg("movino", $path['path'])) {
         
        // Call the callback function implementing the page.
        return call_user_func($path['callback']);
      }
    }
  }
}


/**************************** Handle Drupal API calls ****************************/


function module_implements($hook) {
  return FALSE;
}

function module_exists() {
  return FALSE;
}

function module_invoke_all($hook) {
  switch ($hook) {
    case 'movino_player_supports':
      if (defined('PATH_CORTADO')) {
        return movino_player_cortado_movino_player_supports();
      }
    case 'movino_player':
      if (defined('PATH_CORTADO')) {
        return movino_player_cortado_movino_player();
      }
  }
  
  return;
}

function arg($index) {
  static $arguments, $q;

  if (empty($arguments) || $q != $_GET['q']) {
    $arguments = explode('/', $_GET['q']);
    $q = $_GET['q'];
  }

  if (isset($arguments[$index])) {
    return $arguments[$index];
  }
} 


function t($string, $args = 0) {
  return $string;
}


function l($text, $path, $attributes = array(), $query = NULL, $fragment = NULL, $absolute = FALSE, $html = FALSE) {
  return '<a href="'. url($path, $query, $fragment, $absolute) . '">'. $text .'</a>';
}


function url($path = NULL, $query = NULL, $fragment = NULL, $absolute = FALSE) {
  return $_SERVER['SCRIPT_NAME'] . '?q=' . $path . '&' . $query;
}


function drupal_get_path($type, $id) {
  if ($id == 'movino_player_cortado') {
    return PATH_CORTADO;
  }
  return '.';
}


function base_path() {
}


function drupal_add_css() {
}


function user_access() {
  return TRUE;
}


function drupal_set_title() {
}


function variable_get($id, $alt) {
  global $conf;

  if (!isset($conf)) {
    $conf = parse_ini_file('standalone.ini');
  }
    
  if (!isset($conf[$id])) {
    return $alt;
  }
  return $conf[$id];

}


function theme() {
  $args = func_get_args();
  $function = 'theme_' . array_shift($args);

  return call_user_func_array($function, $args);
}


function drupal_set_message($message = NULL, $type = 'status') {
  echo '<div class="message">' . $message . '</div>';
}


function theme_pager($tags = array(), $limit = 10, $element = 0, $parameters = array()) {
  global $pager_total;
  $output = '';

  if ($pager_total[$element] > 1) {
    $output .= '<div class="pager">';
    $output .= theme('pager_first', ($tags[0] ? $tags[0] : t('first')), $limit, $element, $parameters);
    $output .= theme('pager_previous', ($tags[1] ? $tags[1] : t('previous')), $limit, $element, 1, $parameters);
    $output .= theme('pager_list', $limit, $element, ($tags[2] ? $tags[2] : 9 ), '', $parameters);
    $output .= theme('pager_next', ($tags[3] ? $tags[3] : t('next')), $limit, $element, 1, $parameters);
    $output .= theme('pager_last', ($tags[4] ? $tags[4] : t('last')), $limit, $element, $parameters);
    $output .= '</div>';

    return $output;
  }
} 


function theme_pager_list($limit, $element = 0, $quantity = 5, $text = '', $parameters = array()) {
  global $pager_page_array, $pager_total;

  $output = '<span class="pager-list">';
  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  // When there is more than one page, create the pager list.
  if ($i != $pager_max) {
    $output .= $text;
    if ($i > 1) {
      $output .= '<span class="pager-ellipsis">...</span>';
    }

    // Now generate the actual pager piece.
    for (; $i <= $pager_last && $i <= $pager_max; $i++) {
      if ($i < $pager_current) {
        $output .= theme('pager_previous', $i, $limit, $element, ($pager_current - $i), $parameters);
      }
      if ($i == $pager_current) {
        $output .= '<strong class="pager-current">'. $i .'</strong>';
      }
      if ($i > $pager_current) {
        $output .= theme('pager_next', $i, $limit, $element, ($i - $pager_current), $parameters);
      }
    }

    if ($i < $pager_max) {
      $output .= '<span class="pager-ellipsis"> - </span>';
    }
  }
  $output .= '</span>';

  return $output;
}


function theme_pager_first($text, $limit, $element = 0, $parameters = array()) {
  global $pager_page_array;
  $output = '';

  // If we are anywhere but the first page
  if ($pager_page_array[$element] > 0) {
    $output = theme('pager_link', $text, pager_load_array(0, $element, $pager_page_array), $element, $parameters, array('class' => 'pager-first'));
  }

  return $output;
} 


function theme_pager_next($text, $limit, $element = 0, $interval = 1, $parameters = array()) {
  global $pager_page_array, $pager_total;
  $output = '';

  // If we are anywhere but the last page
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $page_new = pager_load_array($pager_page_array[$element] + $interval, $element, $pager_page_array);
    // If the next page is the last page, mark the link as such.
    if ($page_new[$element] == ($pager_total[$element] - 1)) {
      $output = theme('pager_last', $text, $limit, $element, $parameters);
    }
    // The next page is not the last page.
    else {
      $output = theme('pager_link', $text, $page_new, $element, $parameters, array('class' => 'pager-next'));
    }
  }

  return $output;
} 


function theme_pager_previous($text, $limit, $element = 0, $interval = 1, $parameters = array()) {
  global $pager_page_array;
  $output = '';

  // If we are anywhere but the first page
  if ($pager_page_array[$element] > 0) {
    $page_new = pager_load_array($pager_page_array[$element] - $interval, $element, $pager_page_array);

    // If the previous page is the first page, mark the link as such.
    if ($page_new[$element] == 0) {
      $output = theme('pager_first', $text, $limit, $element, $parameters);
    }
    // The previous page is not the first page.
    else {
      $output = theme('pager_link', $text, $page_new, $element, $parameters, array('class' => 'pager-previous'));
    }
  }

  return $output;
}


function pager_load_array($value, $element, $old_array) {
  $new_array = $old_array;
  // Look for empty elements.
  for ($i = 0; $i < $element; $i++) {
    if (!$new_array[$i]) {
      // Load found empty element with 0.
      $new_array[$i] = 0;
    }
  }
  // Update the changed element.
  $new_array[$element] = (int)$value;
  return $new_array;
}


function theme_pager_last($text, $limit, $element = 0, $parameters = array()) {
  global $pager_page_array, $pager_total;
  $output = '';

  // If we are anywhere but the last page
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $output = theme('pager_link', $text, pager_load_array($pager_total[$element] - 1, $element, $pager_page_array), $element, $parameters, array('class' => 'pager-last'));
  }

  return $output;
}


function theme_pager_link($text, $page_new, $element, $parameters = array(), $attributes = array()) {
  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query[] = drupal_query_string_encode($parameters, array());
  }
  $querystring = pager_get_querystring();
  if ($querystring != '') {
    $query[] = $querystring;
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('first') => t('Go to first page'),
        t('previous') => t('Go to previous page'),
        t('next') => t('Go to next page'),
        t('last') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    else if (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array('@number' => $text));
    }
  }

  return l($text, $_GET['q'], $attributes, count($query) ? implode('&', $query) : NULL);
}


function pager_get_querystring() {
  static $string = NULL;
  if (!isset($string)) {
    $string = drupal_query_string_encode($_REQUEST, array_merge(array('q', 'page'), array_keys($_COOKIE)));
  }
  return $string;
}


function drupal_query_string_encode($query, $exclude = array(), $parent = '') {
  $params = array();

  foreach ($query as $key => $value) {
    $key = drupal_urlencode($key);
    if ($parent) {
      $key = $parent .'['. $key .']';
    }

    if (in_array($key, $exclude)) {
      continue;
    }

    if (is_array($value)) {
      $params[] = drupal_query_string_encode($value, $exclude, $key);
    }
    else {
      $params[] = $key .'='. drupal_urlencode($value);
    }
  }

  return implode('&', $params);
}


function drupal_urlencode($text) {
  if (variable_get('clean_url', '0')) {
    return str_replace(array('%2F', '%26', '%23'),
                       array('/', '%2526', '%2523'),
                       urlencode($text));
  }
  else {
    return str_replace('%2F', '/', urlencode($text));
  }
} 


function format_date($timestamp) {
  return date('Y.m.d H:i:s', $timestamp);
}

?>