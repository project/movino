
Drupal installation:

1. Install Drupal.

2. Extract the package in your Drupal modules directory.

3. Visit the module settings page (admin/build/modules) and activate the modules you need.
   
       Movino API
         - The base functionality
         - Required by all extension modules

       Movino Admin
         - Admin interface for configuration of the Movino API

       Movino Lists
         - Provides a couple of pages and blocks with lists of video content.

       Movino Node
         - Turns Movino content into nodes. Use if you want commenting,
           ability to add description to videos, integration with Views,
           CCK, VotingAPI etc.

       Movino Views
         - Makes Movino content available to the Views module in a more granular way,
           by providing content metadata, preview images and embedded players as Views fields.
         - Requires Movino Node and Views.

       Movino Remote Authentication
         - Allows a Movino server to verify that the video source
           has a valid account at you Drupal site.

4. If you want Movino content to play embedded in the web browser,
   add the "Movino Player: Cortado" module available at http://www.movino.org



Standalone installation:


1. Edit standalone.ini to fit your needs. At least configure a video server.

2. Have a look at standalone.php.example for an example on how to use the Movino web interface without having a Drupal installation.

Note that this only give you the very basic features ie. limited customization, no feed caching etc..
