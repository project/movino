<?php // $Id$
/*
    Movino Web Frontend - Server handling, feed parsing and feed caching
    Copyright 2006, 2007 Tom Sundström

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/


/**
 * Parses all feeds from all servers.
 * Ignores recently updated feeds.
 */
function movino_update_video_cache($type = NULL) {
      
  // If the feed is updated after this point in time, skip it.
  $feed_live_expires = time() - variable_get('movino_feed_age_live', 10);
  $feed_archive_expires = time() - variable_get('movino_feed_age_archive', 300);
  
  $servers = movino_get_servers();
  
  if (empty($servers)) {
    return;
  }
    
  foreach ($servers as $server) {
    $processed = array();
    $skipped = array();
    if (empty($server['feed_live'])) {
      $skipped[] = 'live';
    } else {
      if ($server['feed_live_updated'] > $feed_live_expires || $type == 'archived') {
        $skipped[] = 'live';
      } else {
        movino_server_update_feed_age($server['id'], 'live');
        $feed = _movino_feed_parse(movino_feed_url($server, 'live'));
        if (empty($feed)) {
          // $skipped[] = 'live';        	
        } else {
          foreach ($feed as $video) {
            $video['type'] = 'live';
            $video['server'] = $server['id'];
            if ($cached_video = movino_get_video($video['server'], $video['id'], FALSE, TRUE, $video['username'], $video['created'])) {
              // Update if necessary.
              $processed[] = $cached_video['vid'];
              if ($cached_video['available'] == 0 || $cached_video['type'] != $video['type'] || $cached_video['position'] != $video['position'] || $cached_video['trail'] != $video['trail'] || $cached_video['title'] != $video['title'] || $cached_video['author'] != $video['author'] || (empty($cached_video['preview']) && !empty($video['preview']))) {
                movino_update_video($video);
              }
            } else {
              $processed[] = movino_insert_video($video); 
            }
          }
        }
      }
    }
    if (empty($server['feed_archive'])) {
      $skipped[] = 'archived';
    } else {
      if ($server['feed_archive_updated'] > $feed_archive_expires || $type == 'live') {
        $skipped[] = 'archived';
      } else {
        movino_server_update_feed_age($server['id'], 'archive');
        $feed = _movino_feed_parse(movino_feed_url($server, 'archive'));      
        if (empty($feed)) {
          $skipped[] = 'archived';      	
        } else {
          foreach ($feed as $video) {
            $video['type'] = 'archived';
            $video['server'] = $server['id'];
            if ($cached_video = movino_get_video($video['server'], $video['id'], FALSE, TRUE, $video['username'], $video['created'])) {
              $processed[] = $cached_video['vid'];
              if ($cached_video['available'] == 0 || $cached_video['type'] != $video['type'] || $cached_video['position'] != $video['position'] || $cached_video['trail'] != $video['trail'] || $cached_video['title'] != $video['title'] || $cached_video['author'] != $video['author']) {
                movino_update_video($video);
              }
            } else {
              $processed[] = movino_insert_video($video);
            }
          }
        }
      }
    }
    /*
    global $user;
    if ($user->uid == 1) {
        drupal_set_message('test ' . $server['id']);  
    }
    */
    
    if ((empty($skipped) || !in_array('live', $skipped)) && (variable_get('movino_unpublish', 1) != 0)) {
      /*
      if ($user->uid == 1) {
        drupal_set_message('unpublish ' . $server['id']);  
      }
      */
      // Unpublish all live streams no longer available.
      $sql = "SELECT vid, id, server FROM {movino_videos} WHERE available = 1 AND server = '%s' AND type = 'live' ";
      $additional_conditions = '';
      if (!empty($processed)) {
        $additional_conditions .= 'AND vid != ' . implode(' AND vid != ', $processed) . ' ';
      }
      $results = db_query($sql . $additional_conditions, $server['id']);
      
      while($video = db_fetch_array($results)) {
        /*
        if ($user->uid == 1) {
          drupal_set_message('deactivate: ' . print_r($video, TRUE));
        }
        */
        // Allow other modules to perform actions on inactive videos.
        module_invoke_all('movino_video_deactivate', $video);  
      }
      
      // Mark all inactive videos.
      $sql = "UPDATE {movino_videos} SET available = 0 WHERE type = 'live' AND available = 1 AND server = '%s' ";
      db_query($sql . $additional_conditions, $server['id']);
      
    }
    
    
    /* No need to unpublish archived videos for now...
    if ((empty($skipped) || count($skipped) == 1) && (variable_get('movino_unpublish', 1) != 0)) {
      // Find all videos in cache originating from this server, which were
      // no longer found in the server feeds.
      $sql = "SELECT vid, id, server FROM {movino_videos} WHERE available = 1 AND server = '%s' ";
      $additional_conditions = '';
      if (!empty($processed)) {
        $additional_conditions .= 'AND vid != ' . implode(' AND vid != ', $processed) . ' ';
      }
      if (!empty($skipped)) {
        $additional_conditions .= "AND type != '" . implode("' AND type != '", $skipped) . "' ";
      }
      $results = db_query($sql . $additional_conditions, $server['id']);
      
      while($video = db_fetch_array($results)) {
        // Allow other modules to perform actions on inactive videos.
        module_invoke_all('movino_video_deactivate', $video);  
      }
      
      // Mark all inactive videos.
      $sql = "UPDATE {movino_videos} SET available = 0 WHERE available = 1 AND server = '%s' ";
      db_query($sql . $additional_conditions, $server['id']);
   
     }
     */
  }
}


/**
 * Inserts a new video into DB
 */
function movino_insert_video($video) {
  if (empty($video)) {
    return FALSE;  
  }
  
  $query = <<<END
INSERT INTO {movino_videos} SET
vid = %d,
id = '%s',
server = '%s',
type = '%s',
title = '%s',
author = '%s',
username = '%s',
url = '%s',
size = '%s',
created = %d,
length = %d,
encoding = '%s',
preview = '%s',
position = '%s',
trail = '%s',
available = 1,
published = 1,
removed = 0
END;

  $video['vid'] = db_next_id('{movino_videos}_vid');
  if ($result = db_query($query,
    $video['vid'],
    $video['id'],
    $video['server'],
    $video['type'],
    $video['title'],
    $video['author'],
    empty($video['username']) ? '' : $video['username'],
    $video['url'],
    $video['size'],
    $video['created'],
    $video['length'],
    $video['encoding'],
    $video['preview'],
    $video['position'],
    $video['trail'])) {
      
    // Allow modules to interact when a video has been inserted.
    movino_invoke_all('movino_video_insert', $video);
      
    if (MOVINO_DEBUG) {
      drupal_set_message('Movino video ' . $video['title'] . ' (' . $video['id'] . ') is added.');
    }
    return $video['vid'];
  } else {
    if (MOVINO_DEBUG) {
      drupal_set_message('Unknown error - Movino video ' . $video['title'] . ' (' . $video['id'] . ') not added.', 'error');
    }
    return 0;
  }
}


/**
 * Updates a video in DB
 */
function movino_update_video($video) {
  if (empty($video)) {
    return FALSE;  
  }
    
  $query = <<<END
UPDATE {movino_videos} SET
type = '%s',
title = '%s',
author = '%s',
username = '%s',
url = '%s',
size = '%s',
created = %d,
length = %d,
encoding = '%s',
preview = '%s',
position = '%s',
trail = '%s',
available = 1,
published = 1,
removed = 0
WHERE id = '%s'
AND server = '%s'
END;

  if ($result = db_query($query,
    $video['type'],
    $video['title'],
    $video['author'],
    empty($video['username']) ? '' : $video['username'],
    $video['url'],
    $video['size'],
    $video['created'],
    $video['length'],
    $video['encoding'],
    $video['preview'],
    $video['position'],
    $video['trail'],
    $video['id'],
    $video['server'])) {
      
    // Allow modules to interact when a video has been updated.
    $video = movino_get_video($video['server'], $video['id']);
    module_invoke_all('movino_video_update', $video);
      
    if (MOVINO_DEBUG) {
      drupal_set_message('Movino video ' . $video['title'] . ' (' . $video['id'] . ') is updated.');
    }
    return 1;
  } else {
    if (MOVINO_DEBUG) {
      drupal_set_message('Unknown error - Movino video ' . $video['title'] . ' (' . $video['id'] . ') not updated.', 'error');
    }
    return 0;
  }
}


/**
 * Marks video as unpublished (compare to node status = 0).
 */
function movino_unpublish_video($vid) {
  db_query('UPDATE {movino_videos} SET published = 0 WHERE vid = %d', $vid);    
}


/**
 * Marks video as published (compare to node status = 1).
 */
function movino_republish_video($vid) {
  db_query('UPDATE {movino_videos} SET published = 1 WHERE vid = %d', $vid);    
}


/**
 * Mark video as removed (i.e. not supposed to be returned in searches regardless of publishing status and server availability).
 */
function movino_remove_video($vid) {
  db_query('UPDATE {movino_videos} SET removed = 1 WHERE vid = %d', $vid);  
}


/**
 * Returns a single video.
 */
function movino_get_video_by_vid($vid, $prepare = TRUE, $include_deactivated = TRUE) {
  $sql = 'SELECT * FROM {movino_videos} WHERE vid = %d ' . ($include_deactivated ? '' : 'AND available = 1 AND removed = 0 AND published = 1 ');
  $result = db_query($sql, $vid);
  
  if ($video = db_fetch_array($result)) {
    if (MOVINO_DEBUG && $prepare) {
      drupal_set_message('Loaded single video by vid: '. $video['title'] . ' (' . $video['id'] . ')');
    }
    $video = ($prepare ? _movino_videos_prepare(array($video)) : array($video));
    return array_shift($video);
  }
  return FALSE;
}


/**
 * Returns a single video.
 */
function movino_get_video($server_id, $video_id, $prepare = TRUE, $deactivated = FALSE, $username = NULL, $created = NULL) {
  return movino_get_videos('single', $server_id, $video_id, 0, 0, $prepare, $deactivated, $username, $created);
}


/**
 * Returns a list of videos. Uses cached values when possible.

 * @param string $op
 *   'archived' - return only archived videos
 *   'live'     - return only live videos
 *   'single'   - return a single video
 *   'all'      - return all videos
 * 
 * @param mixed $servers
 *   Restrict search to a single server (string) or multiple servers (array).
 * 
 * @param string $vid
 *   When returning a single video, the video id is required (as well as the server id).
 * 
 * @param integer $limit
 *   Maximum number of videos to return (newest first).
 * 
 * @param integer $offset
 *   Number of newest videos to skip (useful for pagination).
 *
 * @param integer $prepare
 *   Whether or not to execute video preparation hooks.
 */
function movino_get_videos($op = 'all', $servers = NULL, $video_id = NULL, $limit = 0, $offset = 0, $prepare = TRUE, $deactivated = FALSE, $username = NULL, $created = NULL, $max_age = 0) {
    
  if (defined('MOVINO_STANDALONE')) {
    // No DB - no caching - reload feeds once per page load and keep feed contents in memory.
    return movino_get_videos_direct($op, $servers, $video_id, $limit, $offset, $prepare);
  }
    
  if ($op == 'single') {
    $sql = "SELECT * FROM {movino_videos} WHERE server = '%s' AND id = '%s' " . ($deactivated ? '' : 'AND available = 1 AND published = 1 AND removed = 0 ');
    $params = array($servers, $video_id);
    
    if ($username != NULL) {
      if (!is_array($username)) {
      	$username = array($username);
      }
      $sql .= "AND ( ";
      $first = TRUE;
      foreach($username as $name) {
    	if (!$first) {
      		$sql .= "OR ";
      	}
      	$first = FALSE;
	    $sql .= "username = '%s' ";
    	$params[] = $name;
      }
      $sql .= " ) ";
    }
    
    if ($created != NULL) {
      $sql .= "AND created = %d ";
      $params[] = $created;
    }
    
    if ($max_age > 0) {
      $sql .= "AND created >= %d ";
      $params[] = $max_age;
    }
    
    $result = db_query($sql ,$params);
    if ($video = db_fetch_array($result)) {
      if (MOVINO_DEBUG && $prepare) {
        drupal_set_message('Loaded single video: '. $video['title'] . ' (' . $video['id'] . ')');
      }
      $video = ($prepare ? _movino_videos_prepare(array($video)) : array($video));
      return array_shift($video);
    }
    return FALSE;
  }
  
  $sql = 'SELECT * FROM {movino_videos} ' . ($deactivated ? '' : ' WHERE available = 1 AND published = 1 AND removed = 0 ');
  $params = array();
  
  if ($op == 'live' || $op == 'archived') {
    $sql .= "AND type = '%s' ";
    $params[] = $op; 
  }
  /*
  if ($username != NULL) {
    $sql .= "AND username = '%s' ";
    $params[] = $username;
  }
  */
    if ($username != NULL) {
      if (!is_array($username)) {
      	$username = array($username);
      }
      $sql .= "AND ( ";
      $first = TRUE;
      foreach($username as $name) {
    	if (!$first) {
      		$sql .= "OR ";
      	}
      	$first = FALSE;
	    $sql .= "username = '%s' ";
    	$params[] = $name;
      }
      $sql .= " ) ";
    }

  if ($created != NULL) {
    $sql .= "AND created = %d ";
    $params[] = $created;
  }
  
  if ($max_age > 0) {
    $sql .= "AND created >= %d ";
    $params[] = $max_age;
  }
  
  if ($servers != NULL && !is_array($servers)) {
    $servers = array($servers);
  }
  if (!empty($servers)) {
    $sql .= 'AND (';
    $scnt = count($servers);
    foreach($servers as $server) {
      $sql .= "server = '%s' ";
      $params[] = $server;
      $scnt--;
      if ($scnt > 0) {
        $sql .= 'OR ';
      }
    }
    $sql .= ') ';
  }
  
  $sql .= 'ORDER BY created DESC ';
  
  if ($limit > 0) {
    $sql .= 'LIMIT ' . $limit . ' ';
    $params[] = $limit;
  }
  if ($offset > 0) {
    $sql .= 'OFFSET ' . $offset . ' ';
    $params[] = $offset;
  }

  $videos = array();
  $results = db_query($sql, $params);
  while($video = db_fetch_array($results)) {
    $videos[] = $video;
  }
  if (MOVINO_DEBUG) {
    drupal_set_message('Loaded multiple videos: '. count($videos) . ' - ' . $limit);
  }
  return $video = ($prepare ? _movino_videos_prepare($videos) : $videos);
}


/**
 * Alternative way of reading video data.
 * Loads all feeds into memory once per page load.
 * For situations when we don't use a database (standalone version).
 */
function movino_get_videos_direct($op = 'all', $servers = NULL, $video_id = NULL, $limit = 0, $offset = 0, $prepare = TRUE) {
  static $videos = array();
  static $lookup = array();

  // Parse all feeds.
  if (empty($feeds)) {
    
    $server_info = movino_get_servers();
    if (empty($server_info)) {
      return;
    }
    foreach($server_info as $server) {
      if (!empty($server['feed_live'])) {
        $feed = _movino_feed_parse(movino_feed_url($server, 'live'));
        if (!empty($feed) && is_array($feed)) {
          foreach ($feed as $video) {
            $video['type'] = 'live';
            $key = $video['created'] . '-' . $server['id'] . '-' . $video['id'];
            $lookup[$server['id']][$video['id']] = $key;
            $videos[$key] = $video;
            // Instead of implementing hook_movino_video_prepare() for standalone version.
            $videos[$key]['page'] = 'movino/video/' . $video['server'] . '/' . $video['id'];
          }
        }
      }
      if (!empty($server['feed_archive'])) {
        $feed = _movino_feed_parse(movino_feed_url($server, 'archive'));
        if (!empty($feed) && is_array($feed)) {
          foreach ($feed as $video) {
            $video['type'] = 'archived';
            $key = $video['created'] . '-' . $server['id'] . '-' . $video['id'];
            $lookup[$server['id']][$video['id']] = $key;
            $videos[$key] = $video;
            // Instead of implementing hook_movino_video_prepare() for standalone version.
            $videos[$key]['page'] = 'movino/video/' . $server['id'] . '/' . $video['id'];              
          }
        }
      }
    }
    // Sort results.
    krsort($videos);
  }
  
  if ($op == 'single') {
    $videos = $video = ($prepare ? _movino_videos_prepare(array($videos[$lookup[$servers][$video_id]])) : $array($videos[$lookup[$servers][$video_id]]));
    return array_shift($videos);
  } 

  if ($server != NULL && !is_array($servers)) {
    $servers = array($servers);
  }
  
  $results = array();
  $result_count = 0;
  
  foreach($videos as $video) {
    if ($offset > 0) {
      $offset -= 1;
    } else {
      if ($servers == NULL || in_array($video['serverId'], $servers)) {
        if ($op == 'all' || $video['type'] == $op) {
          $results[] = $video;
          if ($limit > 0 && $result_count == $limit) {
            return ($prepare ? _movino_videos_prepare($results) : $results);
          }
        }
      }
    }
  }
  
  return ($prepare ? _movino_videos_prepare($results) : $results);
}


/**
 * Count the number of videos in the cache.
 */
function movino_get_video_count($inactive = FALSE) {
  $result = db_query('SELECT count(*) FROM {movino_videos} ' . (!$inactive ? 'WHERE available = 1 ' : 'WHERE available != 1 '));
  if($count = db_fetch_array($result)) {
    return $count['count(*)'];
  }
  return 0;
}


/**
 * Do some preparations on the videos after loading them from the XML feed.
 */
function _movino_videos_prepare($videos) {
  if (empty($videos)) {
    return $videos;
  }
  
  foreach ($videos as $key => $video) {
    $id = $video['id'];
    
    if (!empty($videos[$key]['size'])) {
      $size = explode('x', $videos[$key]['size']);
      if (intval($size[0]) > 0 && intval($size[1] > 0)) {
        // Split size into width and height
        $videos[$key]['width'] = $size[0];
        $videos[$key]['height'] = $size[1];
      } else {
        $videos[$key]['width'] = 320;
        $videos[$key]['height'] = 240;       
      }
    } else {
      $videos[$key]['width'] = 320;
      $videos[$key]['height'] = 240;
    }
    
    // Allow moduels to customize values.
    movino_invoke_all('movino_video_prepare', $videos[$key]);
    
    // Allow modules to disable video.
    if (!empty($videos[$key]['disabled'])) {
      unset($videos[$key]);
    }
  }
  return $videos;
}
