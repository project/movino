<?php // $Id$


/**
 * Figures out the URL to a Movino feed
 */
function movino_feed_url($server, $type) {
	// Allow for full paths.
	if (substr($server['feed_' . $type . '_path'], 0, 7) == 'http://') {
    return $server['feed_' . $type . '_path'];
	}
	// Default to path constructed from host and port.
  return 'http://' . $server['host'] . ':' . $server['feed_' . $type . '_port'] . '/' . $server['feed_' . $type . '_path'];  
}


/**
 * Takes an XML feed as a string and returns an array.
 *
 * @param string $xmlData
 *   The contents of the XML feed.
 * 
 * @return array
 */
function _movino_feed_parse($url) {
  
  // Experimentally removed in favor of Curl.
  // $data = file_get_contents($url);
  
  $c = curl_init();
  $timeout = 5;
  curl_setopt ($c, CURLOPT_URL, $url);
  curl_setopt ($c, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt ($c, CURLOPT_CONNECTTIMEOUT, $timeout);
  $data = curl_exec($c);
  curl_close($c);  
  
  // Verify that the contents of the data is a Movino feed.
  if (substr($data, 0, 1) != '<') {
  	if (MOVINO_DEBUG) {
      drupal_set_message('Feed fetching failed: ' . $url);
  	}
  	return FALSE;
  }
  
  // Create a parser object.
  $parser = new XML_Unserializer();
  
  // Parse and return an array.
  return $parser->unserialize($data);  
}


/**
 * Feed parser.
 */
class XML_Unserializer {
  var $stack;
  var $arr_output;
  var $null_token = "null";

  function unserialize($str_input_xml) {
    $p = xml_parser_create("");
    xml_parser_set_option($p,XML_OPTION_SKIP_WHITE,1);
    xml_set_element_handler($p, array(&$this, 'start_handler'), array(&$this, 'end_handler'));
    xml_set_character_data_handler($p, array(&$this, 'data_handler'));
    $this->stack = array(
      array(
        'name' => 'document',
        'attributes' => array(),
        'children' => array(),
        'data' => ''
      )
    );
    if (!xml_parse($p, $str_input_xml)) {
      trigger_error(xml_error_string(xml_get_error_code($p)) ."\n". $str_input_xml, E_USER_NOTICE);
      xml_parser_free($p);
      return;
    }
    xml_parser_free($p);

    $tmp = $this->build_array($this->stack[0]);
    if (is_array($tmp) && count($tmp) == 1) {
      $this->arr_output = array_pop($tmp);
    }
    else {
      $this->arr_output = array();
    }
    unset($this->stack);
    return $this->arr_output;
  }

  function get_unserialized_data() {
    return $this->arr_output;
  }

  function build_array($stack) {
    $result = array();
    if (count($stack['attributes']) > 0) {
      $result = array_merge($result, $stack['attributes']);
    }

    if (count($stack['children']) > 0) {
      if (count($stack['children']) == 1) {
        $key = array_keys($stack['children']);
        if ($stack['children'][$key[0]]['name'] === $this->null_token) {
          return NULL;
        }
      }
      $keycount = array();
      foreach ($stack['children'] as $child) {
        $keycount[] = $child['name'];
      }
      if (count(array_unique($keycount)) != count($keycount)) {
        // enumerated array
        $children = array();
        foreach ($stack['children'] as $child) {
          $children[] = $this->build_array($child);
        }
      }
      else {
        // indexed array
        $children = array();
        foreach ($stack['children'] as $child) {
          $children[$child['name']] = $this->build_array($child);
        }
      }
      $result = array_merge($result, $children);
    }

    if (count($result) == 0) {
      return trim($stack['data']);
    }
    else {
      return $result;
    }
  }

  
  function start_handler($parser, $name, $attribs = array()) {
    $token = array();
    $token['name'] = strtolower($name);
    $token['attributes'] = $attribs;
    $token['data'] = '';
    $token['children'] = array();
    $this->stack[] = $token;
  }
  

  function end_handler($parser, $name, $attribs = array()) {
    $token = array_pop($this->stack);
    $this->stack[count($this->stack) - 1]['children'][] = $token;
  }

  
  function data_handler($parser, $data) {
    $this->stack[count($this->stack) - 1]['data'] .= $data;
  }
}


function xml_serialize($tagname, $data) {
  $xml = "<$tagname>";
  if (is_array($data)) {
    foreach ($data as $key => $value) {
      $xml .= xml_serialize($key, $value);
    }
  }
  else {
    $xml .= "<![CDATA[".$data."]]>";
  }
  $xml .= "</$tagname>\n";
  return $xml;
}
