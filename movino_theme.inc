<?php // $Id$
/*
    Movino Web Frontend - Theme functions
    Copyright 2006, 2007 Tom Sundström

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/


/**
 * Theme function for embedded video teasers.
 *
 * @param array $video
 *   Array with video information
 * @param boolean $page
 *   Whether this is a video page or a video teaser
 * @param array $metadata_enabled
 *   An array with id:s for all metadata fields that should be displayed
 * @param boolean $metadata_labels
 *   Whether or not to show labels above metadata fields
 * 
 * @return string of HTML
 */
function theme_movino_content($video, $page = FALSE, $metadata_enabled = NULL, $metadata_labels = NULL) {
  
  $output = '';
  
  if ($metadata_enabled === NULL) {
    $metadata_enabled = movino_metadata_enabled(!$page);
  }
  if ($metadata_labels === NULL) {
    $metadata_labels = movino_metadata_labels_enabled(!$page);
  }
   
  $metadata_labels = movino_metadata(!$page);
  $metadata_labels_enabled = movino_metadata_labels_enabled(!$page);
  $metadata = '';
        
  if(!empty($metadata_enabled)) {
  
  	if (variable_get('movino_display_movino_metadata_button', 1) && $metadata_enabled['player']) {
  		$metadata_enabled['movino_button'] = 1;
  	}
  
    foreach($metadata_enabled as $key => $field) {
      if ($field == 1) {
        $field = $key;
      }

      $field_content = '';
      
      if (!empty($field)) {
        switch($field) {
        
          case 'player':
          	if ($page && variable_get('movino_display_player_override_size_page', 1)) {
              $video['width'] = variable_get('movino_display_player_width_page', 320);
              $video['height'] = variable_get('movino_display_player_height_page', 240);
          	}
          	if (!$page && variable_get('movino_display_player_override_size_teaser', 1)) {
              $video['width'] = variable_get('movino_display_player_width_teaser', 320);
              $video['height'] = variable_get('movino_display_player_height_teaser', 240);
          	}
            $field_content = movino_player($video);
            break;

          case 'icon':
            if (empty($metadata_enabled['player'])) {
              $field_content = l(theme('movino_video_icon', $video), $video['page'], array('title' => _movino_video_title($video, FALSE)), NULL, NULL, FALSE, TRUE);
            }
            break;

          case 'title':
             $field_content = l(_movino_video_title($video), $video['page'], array('title' => _movino_video_title($video, FALSE)));
            break;
            
          case 'author':
             $field_content = $video['author'];    
            break;
        
          case 'created':
             $field_content = format_date($video['created']);
            break;

          case 'length':
            $field_content = theme('movino_content_length', $video);
            break;
          
          case 'url':
             $field_content = '<a href="'.$video['url'].'">'.$video['url'].'</a>';            
            break;
            
          case 'movino_button':
          	$field_content = '<a href="http://www.movino.org" target="_blank"><img style="margin: 10px 0px; " src="' . base_path() . drupal_get_path('module', 'movino') . '/button.png" alt="Powered by Movino" title="Powered by Movino" /></a>';

          default:
             if (isset($video[$field])) {
               $field_content = $video[$field];        
             }
        }
      }
            
      // Add wrapper, but only if the field is non-empty.
      if (!empty($field_content)) {
        $metadata .= '<div class="movino-field movino-field-' . $field . '">';
        
        // Add a label.
        if (!empty($metadata_labels_enabled[$field])) {
           $metadata .= '<div class="movino-field-label">' . $metadata_labels[$field] . '</div>';
        }
        
        // Add the data.
        $metadata .= '<div class="movino-field-content">' . $field_content . '</div>';
        $metadata .= '</div>';
      }
    }
  }

  return '<div class="movino-content movino-content-' . ($page ? 'page' : 'teaser') . '">' . $metadata . '</div>';  
}


/**
 * Theme function for embedded for video icons.
 */
function theme_movino_video_icon($video) {
  if (!empty($video['icon_width']) && !empty($video['icon_height'])) {
    $width = $video['icon_width'];
    $height = $video['icon_height'];
  } else {
    $width = variable_get('movino_display_preview_width', 176);
    $height = variable_get('movino_display_preview_height', 144);
  }
  
  if (variable_get('movino_display_archive_previews', TRUE) && !empty($video['preview'])) {
    $icon_url = $video['preview'];
  } else {
    $icon_url = base_path() . drupal_get_path('module', 'movino') . '/preview/' . variable_get('movino_display_preview_default_image', 'movino-logo.png');
  }
        
  return '<img class="movino-video-icon" src="' . $icon_url . '"' .
           (empty($width) ? '' : ' width="' . $width . '"') .
           (empty($height) ? '' : ' height="' . $height . '"') .
         '/>';
}


/**
 * Theme function for video length values.
 */
function theme_movino_content_length($video) {
  
  // Do not show length for live content.
  if ($video['type'] == 'live') {
     return;
  }
  $hours = floor($video['length'] / 3600);
  $minutes = floor(($video['length'] % 3600) / 60);
  $seconds = $video['length'] % 60;
          
  return (!empty($hours) ? $hours . ':' : '')
         . ($minutes < 10 ? '0' : '') . $minutes . ':'
         . ($seconds < 10 ? '0' : '') . $seconds;  
}
