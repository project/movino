<?php // $Id$
/*
    Movino Web Frontend
    Copyright 2006, 2007 Tom Sundström

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/


/**
 * Implementation of hook_help().
 */
function movino_node_help($section) {
  switch ($section) {
    case 'admin/help#help':
      // TODO: expand
      $output = '<p>'. t('Turns Movino content into Drupal nodes. Useful for interactivity such as commenting and voting as well as for creting customized lists with the Views module.') . '</p>';
      return $output;
  }
}


/*************************** MOVINO HOOKS ******************************/


/**
 * Implementation of hook_movino_video_prepare().
 */
function movino_node_movino_video_prepare(&$video) {
  
  if (movino_node_deleted($video['vid'])) {
    $video['disabled'] = TRUE;
  }
  
  // Add node specific video info.
  $nid = movino_node_get_video_nid($video['vid']);
  if (!empty($nid)) {
    $node = node_load($nid);
    $video['nid'] = $nid;
    $video['page'] = 'node/' . $nid;
    $video['title'] = $node->title;
    // $video['author'] = theme('username', $node);
  }
  
  // Optional: add more actions here.
}


/**
 * Implementation of hook_movino_video_insert().
 */
function movino_node_movino_video_insert($video) {
  if (MOVINO_DEBUG) {
    drupal_set_message('Movino Node: insert node: ' . $video['title'] . ' (' . $video['vid']. ')');
  }
  
  // Do not add deleted videos.
  if (movino_node_deleted($video['vid'])) {
    return;
  }
  
  // Create node.
  $nid = FALSE;
              
  $node = (object) NULL;
  $node->type = 'movino_node';
  $node->title = $video['title'];
  $node->body = ' '; // Empty description.
  $node->teaser = ' '; // Empty teaser.
  $node->created = $video['created']; // Use create time of the video server.

  // Optional: add more actions here.

  // Code from node_form() that populates the "Publishing options" checkboxes.
  $node_options = variable_get('node_options_'. $node->type, array('status' => 1, 'promote' => 1, 'comment' => 2));
  $node_options['status'] = 1; // Always publish nodes.
    
  // If this is a new node, fill in the default values.
  foreach (array('status', 'promote', 'sticky') as $nkey) {
    $node->$nkey = (empty($node_options[$nkey]) ? 0 : 1);
  }
    
  // Code from node_submit() that populates the uid field.
  if (!empty($video['username']) && $account = user_load(array('name' => $video['username']))) {
    $node->uid = $account->uid;
  }
  else {
    $node->uid = 1;
  }

  $node->comment = 2;

  node_save($node);
  _movino_node_set_video_nid($video['vid'], $node->nid);
      
  if (MOVINO_DEBUG) {
    drupal_set_message('Movino node saved.');
  }
}


/**
 * Implementation of hook_movino_video_update().
 */
function movino_node_movino_video_update($video) {
  if (MOVINO_DEBUG) {
    drupal_set_message('Movino Node: activate / update node: ' . $video['title'] . ' (' . $video['vid'] . ')' . print_r($video, TRUE));
  }
  
  // Do not udpate deleted videos.
  if (movino_node_deleted($video['vid'])) {
    return;
  }  
  
  $nid = movino_node_get_video_nid($video['vid']);
  if (empty($nid)) {
    if (MOVINO_DEBUG) {
      drupal_set_message('Video nid for ' . $video['vid'] . ' not found.');
    }
    // Not found.
    
    return FALSE;
  }
  
  // Update node.
  
  $node = node_load(array('nid' => $nid));
  if (empty($node)) {
    if (MOVINO_DEBUG) {
      drupal_set_message('Could not load node ' . $nid);
    }
    return FALSE;
  }
  if (MOVINO_DEBUG) {
    drupal_set_message('Updating node ' . $nid);
  }
  $node->status = 1; // Make sure the node is published.
   
  // Optional: add more actions here.
   
  // Save the node to DB.
  node_save($node);  
}


/**
 * Implementation of hook_movino_video_deactivate().
 */
function movino_node_movino_video_deactivate($video) {  
  if (MOVINO_DEBUG) {
    drupal_set_message('Movino Node: deactivate node - video vid = ' . $video['vid']);
  }
  $nid = movino_node_get_video_nid($video['vid']);
  if (!empty($nid)) {
    $node = node_load($nid);
    $node->status = 0; // Make sure the node gets unpublished.
   
    // Optional: add more actions here.
   
    // Save the node to DB.
    node_save($node);    
  }
}


/*************************** DRUPAL NODE HOOKS ******************************/


/**
 * Implementation of hook_node_info().
 */
function movino_node_node_info() {
  return array(
    'movino_node' => array(
      'name' => t('Movino content'),
      'module' => 'movino_node',
      'description' => t("Movino content"),
    )
  );   
}


/**
 * Implementation of hook_access().
 */
function movino_node_access($op, $node) {
  
  global $user;
  
  if ($user->uid == 1) {
    return TRUE;  
  }
  
  switch ($op) {
    case 'create':
      // Never allow users to create Movino nodes.
      // All nodes should be automatically created based on video server feed info.
      return FALSE;
 
    case 'update':
      if (user_access('edit own Movino content') && ($user->uid == $node->uid)) {
        return TRUE;
      }
      return FALSE;
      
    case 'delete':
      // Never allow users to delete Movino nodes, as they would be re-generated automatically
      return FALSE;

    case 'view':
      if (movino_node_is_live($node->nid)) {
        if (user_access('view live Movino content')) {
          return TRUE;
        }
      } elseif (user_access('view archived Movino content')) {
        return TRUE;
      }      
      return FALSE;
  }
  
  return FALSE;
}


/**
 * Implementation of hook_form().
 */
function movino_node_form(&$node) {
  $type = node_get_types('type', $node);

  $video = movino_node_get_video($node->nid);
  $content_info = theme('movino_content', $video, FALSE);
   
  if (!empty($content_info)) {
    $form['movino_content_info'] = array(
      '#type' => 'item',
      '#title' => t('Movino content'),
      '#value' => $content_info . '<div style="clear: both; "></div>', // TODO: remove <div> at some point.
      '#weight' => -6,  
    );
  }
  
  $form['title'] = array(
    '#type'=> 'textfield',
    '#title' => check_plain($type->title_label),
    '#default_value' => $node->title,
    '#description' => t('Tip: You can set the title from your phone before starting the broadcast, by going to <em>Options &raquo; Settings &raquo; Title</em> in the phone app.'),
    '#required' => TRUE,
    '#weight' => -5,
  );
  
  /*
  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => check_plain($type->body_label),
    '#rows' => 20,
    '#default_value' => $node->body,
    '#required' => TRUE,
  ); 
  */
  
  return $form;
}


/**
 * Implementation of hook_delete().
 */
function movino_node_delete(&$node) {
  if ($video = movino_node_get_video($node->nid)) {
    // Mark as deleted
    db_query('UPDATE {movino_node_mapping} SET deleted = 1 WHERE nid = %d', $node->nid);
    movino_remove_video($video['vid']);
  }
}


/**
 * Implementation of hook_update();
 */
function movino_node_update($node) {
  if ($video = movino_node_get_video($node->nid)) {

    if ($node->status) {
      movino_republish_video($video['vid']);
    } else {
      movino_unpublish_video($video['vid']);
    }
  }
}


/**
 * Implementation of hook_view().
 */
function movino_node_view(&$node, $teaser = FALSE, $page = FALSE) {
  
  if (movino_node_is_live($node->nid)) {
    if (!user_access('view live Movino content')) {
      return $node;
    }
  } elseif (!user_access('view archived Movino content')) {
    return $node;
  }
  
  $video = movino_node_get_video($node->nid);
      
  if (!empty($video)) {
      
    if ($page) {
      $node->content['movino'] = array(
        '#value' => theme('movino_content', $video, TRUE),
        '#weight' => -5,
      );
    } 
    if($teaser) {
      $node->content['movino'] = array(
        '#value' => theme('movino_content', $video, FALSE),
        '#weight' => -5,
      );
    }  
  }
 
  return $node;
}


/*************************** DATABASE FUNCTIONS ******************************/


/**
 * Checks whether the given node's video is live or in the archive.
 */
function movino_node_is_live($nid) {
  $result = db_query("SELECT nid FROM {movino_node_mapping} n LEFT JOIN {movino_videos} v ON n.vid = v.vid WHERE v.type = 'live' AND n.deleted != 1 AND n.nid = %d", $nid);
  if ($node = db_fetch_array($result)) {
    return TRUE;
  }
  return FALSE;
  
}


/**
 * Returns true if a mapping between the video and a node has been set up,
 * Returns true even after the node is deleted, so that it's possible to
 * delete videos from the site without deleting them from the server.
 */
function movino_node_deleted($vid) {

  $result = db_query("SELECT nid FROM {movino_node_mapping} WHERE deleted = 1 AND vid = %d ", $vid);
  if ($node = db_fetch_array($result)) {
    return TRUE;
  }
  return FALSE;
}


/**
 * Returns the movino_node nid for a video.
 */
function movino_node_get_video_nid($vid) {

  $result = db_query("SELECT nid FROM {movino_node_mapping} WHERE deleted != 1 AND vid = %d ", $vid);
  if ($node = db_fetch_array($result)) {
    return $node['nid'];
  }
  return FALSE;
}


/**
 * Returns the video metadata given a node id.
 */
function movino_node_get_video($nid) {
  $nodemapping = db_fetch_array(db_query('SELECT v.id, v.server FROM {movino_videos} v LEFT JOIN {movino_node_mapping} n ON n.vid = v.vid WHERE n.deleted != 1 AND n.nid = %d ', $nid));  

  if (empty($nodemapping['id']) || empty($nodemapping['server'])) {
    return FALSE;
  }
  $video = movino_get_video($nodemapping['server'], $nodemapping['id'], TRUE, TRUE);
  if (empty($video)) {
     return FALSE;
  }
  return $video;
}


/**
 * Maps a movino_node to the movino_video table.
 */
function _movino_node_set_video_nid($vid, $nid) {

  $result = db_query("INSERT INTO {movino_node_mapping} SET nid = %d, vid = %d ", $nid, $vid);
  
}
