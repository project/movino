<?php
// ; $Id $
/*
    Movino Player: Flash
    Copyright 2007 Tom Sundström

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/


/**
 * Implementation of hook_help().
 */
function movino_player_flv_help($section) {
  switch ($section) {
    case 'admin/help#help':
      // TODO: expand
      $output = '<p>'. t('Provides a player for Flash video.') . '</p>';
      return $output;
  }
}


/**
 * Implementation of hook movino_player().
 */
function movino_player_flv_movino_player() {
  $players = array();
  
  $players['flv'] = array(
    '#name' => t('Flash video'),
    '#description' => t('Flash video player.'),
    '#callback' => 'movino_player_flv_callback',
    '#settings' => 'admin/settings/movino/players/flv',
    '#supports' => array('flv/mp3/h263'),
  );
  
  return $players;
}


/**
 * Implementation of hook movino_player_supports().
 */
function movino_player_flv_movino_player_supports() {
  $types = array();
  $types['flv/mp3/h263'] = 'flv';
  return $types;
}


/**
 * Generates the HTML code required to display the video player.
 */
function movino_player_flv_callback($video) {  
    
 	global $user;
  $module_path = drupal_get_path('module', 'movino_player_flv');
  $swf_filename = variable_get('movino_player_flv_swf', '');
  
  // drupal_set_message('swf: ' . $swf_filename);
  
  if (empty($swf_filename)) {
  	  return theme('movino_player_flv_error', t('No flash player installed.') 
	  	. ' '
	  	. (user_access('administer Movino')
	  	? l('Install and set up a flash player', 'admin/settings/movino/players/flv')
	  	: t('Contact the site administrator.')
	  )
	  , $video);
  }
  
  
  if (!is_file($module_path . '/' . $swf_filename)) {
	  return theme('movino_player_flv_error', t('Flash player missing.') 
	  	. ' '
	  	. (user_access('administer Movino')
	  	? l('Install and set up a flash player', 'admin/settings/movino/players/flv')
	  	: t('Contact the site administrator.')
	  )
	  , $video);
  }
  
  $video['swf'] = base_path() . $module_path . '/' . $swf_filename;
      
  return theme('movino_player_flv_embed', $video);
}


function theme_movino_player_flv_embed($video) {
  
  $path = drupal_get_path('module', 'movino_player_flv');
  $autoplay = ($video['type'] == 'live' ? 'true' : 'false');
  $flashVars = array();
    
  // JW FLV Media Player parameters
  $flashVars['file'] = $video['url'];
  if (!empty($video['preview']) && $autoplay == 'false') {
	  $flashVars['image'] = $video['preview'];
  }
  $flashVars['autostart'] = $autoplay;
  $flashVars['shownavigation'] = ($video['type'] == 'live' ? 'false' : 'true');
  $flashVars['bufferlength'] = 0;
  
  if (variable_get('movino_display_movino_overlay_logo', 1)) {
  	// Add overlay.
	  $flashVars['logo'] = base_path() . drupal_get_path('module', 'movino') . '/logo-overlay.png';
  	$flashVars['link'] = 'http://www.movino.org';
	  $flashVars['linkfromdisplay'] = 'true';
  	$flashVars['linktarget'] = '_blank';
  }
  
  // OS FLV parameters
  $flashVars['movie'] = $video['url'];
  $flashVars['autoplay'] = $autoplay;
  
  // Generate FlashVars-string.
  $fv = '';
  foreach($flashVars as $key => $val) {
     if (!empty($fv)) {
       $fv .= '&';
     }
     $fv .= urlencode($key) . '=' . urlencode($val);
  }
  
  // Generate embed code.
  $output = '<object id="movino-player-flv" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' . $video['width'] . '" height="' . $video['height'] . '"><param name="movie" value="' . $video['swf'] . '"></param><param name="flashVars" value="' . $fv . '"></param><param name="quality" value="high"></param><param name="allowfullscreen" value="true"></param><embed name="movino-player-flv" src="' . $video['swf'] . '" type="application/x-shockwave-flash" flashVars="' . $fv . '" width="' . $video['width'] . '" height="' . $video['height'] . '" quality="high" allowfullscreen="true"></embed></object>';

  return $output;
}


function theme_movino_player_flv_error($error_msg, $video) {
 	if ($video['width'] && $video['height']) {
		return '<div class="error" style="width: ' . $video['width']. 'px; height: ' . $video['height']. 'px; ">' . $error_msg . '</div>';
 	}
	return '<div class="error">' . $error_msg . '</div>';
}


/**
 * Implementation of hook_menu().
 */
function movino_player_flv_menu($may_cache) {
  $items = array();
  
  if (!$may_cache) {

    $items[] = array(
      'path' => 'admin/settings/movino/players/flv',
      'title' => t('Movino Video Player: Flash'),
      'type' => MENU_CALLBACK,
      'callback' => 'drupal_get_form',
      'callback arguments' => array('movino_player_flv_admin_form'),
      'access' => user_access('administer Movino'),
    );
  }

  return $items;
}


/**
 * Movino admin settings - embedded player tab
 */
function movino_player_flv_admin_form() {

  $available_swfs = _movino_player_flv_get_swfs();
  $module_path = drupal_get_path('module', 'movino_player_flv');

  $form['flv'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select video player'),
    '#collapsible'  => FALSE,
    '#collapsed' => FALSE,
    '#description' => (empty($available_swfs)
	  	? '<div class="error"><strong>' . t('No flash video player uploaded!') . '</strong>'
	  	. '<br />' . t('You need to upload a flash video player into a subdirectory of <em>@path</em> to enable flash based video playback.', array('@path' => $module_path))
	  	. '</div>'

	  	: '<p>'
	  	. t('You can install additional players by uploading them to a subdirectory of @path.', array('@path' => $module_path))
	  	. '</p>'
	  ),
  );
  
  $form['flv']['supported_players'] = array(
    '#type' => 'fieldset',
    '#title' => t('The following video players are tested'),
    '#collapsible'  => TRUE,
    '#collapsed' => !empty($available_swfs),
    '#description' => '<ul>'
 		 		
 		. '<li><a href="http://www.jeroenwijering.com/?item=JW_FLV_Player">JW FLV Media Player</a>'
 		. '<br />Tested version: 3.16'
 		. '<br />License: Creative Commons Non-commercial</li>'
 	
 		. '<li><a href="http://www.osflv.com/">OS FLV</a>'
 		. '<br />Tested version: 2.0.5'
 		. '<br />License: GPL</li>'
 		
 		. '</ul>');

	$available_swfs[''] = '--- ' . t('Select video player') . ' ---';
	ksort($available_swfs);
	
  $form['flv']['movino_player_flv_swf'] = array(
    '#type' => 'select',
    '#title' => t('Select which video player to use'),
    '#options' => $available_swfs,
    '#default_value' => variable_get('movino_player_flv_swf', ''),
  );
  	  
  return system_settings_form($form);
}

function _movino_player_flv_get_swfs($path = NULL, $prefix = '') {

	if ($path == NULL) {
	  $path = drupal_get_path('module', 'movino_player_flv');
	}
	$result = array();
	$subdirs = array();
	
	$fh = opendir($path);
	while ($file = readdir($fh)) {
  	if ($file != "." && $file != "..") {
	    if(is_dir($path."/".$file)) {
	    	// Found subdirectory.
	    	$subdirs[] = $file;
 	  	} elseif (strtolower(substr($file, strlen($file) - 4)) == '.swf') {
 	  		// Found .SWF-file
 	  		$result[$prefix . $file] = $prefix . $file;
 	  	}
	  }
	}
	closedir($fh);
	
	if (!empty($subdirs)) {
		foreach($subdirs as $dir) {
			$subresult = _movino_player_flv_get_swfs($path . '/' . $dir, $dir . '/');
			if (!empty($subresult)) {
				$result = array_merge($result, $subresult);
			}
		}
	}
	
	return $result;
}