<?php // $Id$
/*
    Movino Web Frontend - Integration with external players
    Copyright 2006, 2007 Tom Sundström

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/


/**
 * Returns an array containing information on all
 * installed embedded players compattible with Movino.
 * 
 * Example:
 * 
 *   $players['cortado'] = array(
 *    '#name' => t('Cortado'),
 *    '#callback' => 'movino_player_cortado_callback',
 *  );
 *
 * @return array
 */
function movino_player_list() {
  return module_invoke_all('movino_player');
}


/**
 * Display a video by embedding a player.
 * Select which of the installed player to use
 * by looking at the 'encoding' parameter of the video.
 *
 * @param array $video
 * @return string of HTML
 */
function movino_player($video) {

  // Figure out what formats the installed players support.
  $support = module_invoke_all('movino_player_supports');

  if (isset($support[$video['encoding']])) {
    $players = movino_player_list();
    $callback = $players[$support[$video['encoding']]]['#callback'];
    return call_user_func($callback, $video);
  }

  // Did not find a suitable player - return an unsupported message.
  return theme('movino_player_unsupported', $video);
}


/**
 * Theme a generic embedded player
 *
 * @param array $video
 * @param array $extra_params
 */
function theme_movino_player_embed($player_name, $params, $extra_params = array()) {
  $extra_params_embed = '';
  $extra_params_object = '';
  
  if (!empty($extra_params)) {
    foreach($extra_params as $pname => $pval) {
      $extra_params_embed .= "\n           $pname=\"$pval\"";
      $extra_params_object .= "\n  <param name=\"$pname\" value=\"$pval\"/>";
    }
  }
    
  // Read resize parameters.
  if (movino_player_embed_allow_resize($params)) {
    if (isset($_REQUEST['movino-ps'])
        && $_REQUEST['movino-ps'] >= movino_player_embed_scale_min()
        && $_REQUEST['movino-ps'] <= movino_player_embed_scale_max()
        )
    {    
      $params['width'] = $_REQUEST['movino-ps'] * $params['width'];
      $params['height'] = $_REQUEST['movino-ps'] * $params['height'];
    }
  }
  
  $size_attrib = ' style="width: '. $params['width'] . 'px; height: '. $params['height'] .'px; "';
    
  $output = <<<END
<div id="movino-player"{$size_attrib}>
<object id="{$player_name}-object"
        classid="{$params['classid']}"
        width="{$params['width']}"
        height="{$params['height']}"
        align="{$params['align']}"
        code="{$params['code']}"
        codebase="{$params['codebase']}"
>      
  <param name="archive" value="{$params['archive']}"/>
  <param name="url" value="{$params['url']}"/>{$extra_params_object}
  
  <comment>
    <embed id="cortado-embed" type="application/x-java-applet"
           width="{$params['width']}"
           height="{$params['height']}"
           align="{$params['align']}"
           code="{$params['code']}"
           archive="{$params['archive']}"
           codebase="{$params['codebase']}"
           url="{$params['url']}"{$extra_params_embed}
      >
      <noembed>You need Java to view this media file.</noembed>
    </embed>
  </comment>
</object>
</div>
END;

  if (movino_player_embed_allow_resize($params)) {
    $output .= theme('movino_player_embed_resize', $params);
  }
    
  return $output;
}


/**
 * Theme embed resize control.
 */
function theme_movino_player_embed_resize($video) {

  $output = '';
  $ps = (isset($_REQUEST['movino-ps']) ? $_REQUEST['movino-ps'] : 1);
  
  if ($ps > movino_player_embed_scale_min()) {
    $output .= l('-', $_REQUEST['q'], NULL, 'movino-ps=' . ($ps - 1));
  }
  if ($ps < movino_player_embed_scale_max()) {
    $output .= l('+', $_REQUEST['q'], NULL, 'movino-ps=' . ($ps + 1));
  }
  
  if (empty($output)) {
    return '';
  }
    
  return '<div id="resize-larger" style="margin-left: ' . $video['width'] . 'px ">' . $output . '</div>';
}


/**
 * Theme the 'unsupported video format' message.
 */
function theme_movino_player_unsupported($video) {
  return '<div id="movino-player" class="error">'. t('No video player installed for @encoding content.', array('@encoding' => $video['encoding'])) 
	 	. ' '
	 	. (user_access('administer Movino')
	  	? t('<a href="http://drupal.org/project/movino">Find</a> (or implement) a Movino-targeted Drupal module providing this functionality, then <a href="' . url('admin/build/modules') . '">install</a> and <a href="' . url('admin/settings/movino/players') . '">configure</a> it.')
	  	: t('Contact the site administrator.')
	 	).'</div>';
}
