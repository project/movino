<?php // $Id$
/*
    Movino Web Frontend - Server handling, feed parsing and feed caching
    Copyright 2006, 2007 Tom Sundström

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/


/**
 * Loads the server info from either the Drupal admin interface or the .ini file.
 * 
 * @return array of server-arrays.
 */
function movino_get_servers() {

  $servers = array();

  if (defined('MOVINO_STANDALONE')) {
    // Servers are defined in the standalone.ini file.
    $settings = parse_ini_file('standalone.ini', TRUE);
    $server_ids = explode(',', $settings['movino_video_servers']);
    if (empty($server_ids)) {
      return array();
    }
    $servers = array();
    foreach ($server_ids as $server_id) {
      $server_id = trim($server_id);
      $servers[$server_id] = $settings[$server_id];
      $servers[$server_id]['id'] = $server_id;
    }
    return $servers;
  }

  // Servers are defined in the movino_servers DB table.
  // Load server info from db
  $result = db_query("SELECT * FROM {movino_servers} ");

  while($server = db_fetch_array($result)) {
     $servers[$server['id']] = $server;
     $servers[$server['id']]['admin_links'] = l(t('Edit'), 'admin/settings/movino/servers/edit/' . $server['id']) . ' | ' . l(t('Delete'), 'admin/settings/movino/servers/delete/' . $server['id']);
  }
  
  // Allow third party modules to define servers.
  $additional_servers = module_invoke_all('movino_servers');

  return array_merge($additional_servers, $servers);
}


/**
 * Generates a unique secret key for server $id and stores it in DB.
 */
function movino_server_generate_secret_key($id) {

  usleep(rand(0, 100));
  $key = md5(microtime());
  
  $result = db_query("SELECT * FROM {movino_servers} WHERE secret_key = '%s' ", $key);
  if ($record = db_fetch_array($result)) {
    // Key already used, generate a new one
    return movino_server_generate_secret_key($id);  
  }
  
  movino_server_update_secret_key($id, $key);
  
  return $key;
}

/**
 * Stores a secret video server key in DB.
 */
function movino_server_update_secret_key($id, $key) {
  $query = "UPDATE {movino_servers} SET secret_key = '%s' WHERE id = '%s'";
  $result = db_query($query, $key, $id);

}


/**
 * Returns server id for the given secret key. Returns false if no match.
 */
function movino_server_validate_secret_key($key) {
  $result = db_query("SELECT * FROM {movino_servers} WHERE secret_key = '%s' ", $key);
  if ($server = db_fetch_array($result)) {
    // Key already used, generate a new one
    return $server['id']; 
  }
  
  return FALSE;
}


/**
 * Get Movino server info.
 *
 * @param integer $id The id of the server.
 * @return boolean
 */
function movino_server_get($id) {
  
  if (defined('MOVINO_STANDALONE')) {
    // Servers are defined in the standalone.ini file.
    $settings = parse_ini_file('standalone.ini');
    if (empty($settings[$id])) {
      return array();
    }
    return $settings[$id];
  }
  
  // Load server info from db
  $result = db_query("SELECT * FROM {movino_servers} WHERE id = '%s' ", $id);

  if (db_num_rows($result)) { 
    $server = db_fetch_array($result);  
  }
  
  if (empty($server)) {
    return FALSE;
  }
  return $server;
}


/**
 * Insert a Movino server into DB.
 *
 * @param array $server
 * @return boolean
 */
function movino_server_insert($server) {
  
  // Fallback to defaults.
  $server = array_merge(_movino_server_settings_defaults(), $server);

  if (movino_server_get($server['id'])) {
    drupal_set_message('Server id already exists. Server not created!', 'error');
    return FALSE;
  }
  
  $query = <<<END
INSERT INTO {movino_servers} SET
id = '%s',
name = '%s',
description = '%s',
host = '%s',
feed_live = %d,
feed_live_port = %d,
feed_live_path = '%s',
feed_live_updated = %d,
feed_archive = %d,
feed_archive_port = %d,
feed_archive_path = '%s',
feed_archive_updated = %d,
source_port = %d
END;

  if ($result = db_query($query,
    $server['id'],
    $server['name'],
    $server['description'],
    $server['host'],
    $server['feed_live'],
    $server['feed_live_port'],
    $server['feed_live_path'],
    $server['feed_live_updated'],
    $server['feed_archive'],
    $server['feed_archive_port'],
    $server['feed_archive_path'],
    $server['feed_archive_updated'],
    $server['source_port'])) {
      
      // Allow modules to interact when a server has been inserted.
      module_invoke_all('movino_server_insert', $server);
      
      drupal_set_message('Movino server ' . $server['name'] . ' (' . $server['id'] . ') is added.');
      return TRUE;
    } else {
      drupal_set_message('Unknown error - Movino server ' . $server['name'] . ' (' . $server['id'] . ') not added.', 'error');
      return FALSE;
    }
    
}


/**
 * Update a Movino server already in DB.
 *
 * @param array $server
 * @return boolean
 */
function movino_server_update($server) {
  
  // Fallback to defaults.
  $server = array_merge(_movino_server_settings_defaults(), $server);

  if (!movino_server_get($server['id'])) {
    drupal_set_message('Server id does not exists.', 'error');
    return FALSE;
  }
  
  $query = <<<END
UPDATE {movino_servers} SET
id = '%s',
name = '%s',
description = '%s',
host = '%s',
feed_live = %d,
feed_live_port = %d,
feed_live_path = '%s',
feed_archive = %d,
feed_archive_port = %d,
feed_archive_path = '%s',
source_port = %d
WHERE id = '%s'
END;

  if ($result = db_query($query,
    $server['id'],
    $server['name'],
    $server['description'],
    $server['host'],
    $server['feed_live'],
    $server['feed_live_port'],
    $server['feed_live_path'],
    $server['feed_archive'],
    $server['feed_archive_port'],
    $server['feed_archive_path'],
    $server['source_port'],
    $server['id_old'])) {

      // Allow modules to interact when a server has been updated.
      module_invoke_all('movino_server_update', $server);
      
      drupal_set_message('Movino server ' . $server['name'] . ' (' . $server['id'] . ') is updated.');
      return TRUE;
      
    } else {
      drupal_set_message('Unknown error - Movino server ' . $server['name'] . ' (' . $server['id'] . ') not updated.', 'error');
      return FALSE;
    }
}


/**
 * Update a Movino server already in DB.
 *
 * @param array $server
 * @return boolean
 */
function movino_server_update_feed_age($server_id, $type = 'all') {
    
  $query = 'UPDATE {movino_servers} SET ';
  
  if ($type != 'archive') {
    $query .= 'feed_live_updated = ' . time() . ' ';
  }
  if ($type != 'live') {
    $query .= 'feed_archive_updated = ' . time() . ' ';
  }
  
  $query .= "WHERE id = '%s' ";

  if ($result = db_query($query, $server_id)) {
    if (MOVINO_DEBUG) {
      drupal_set_message('Movino server ' . $server_id . ' - ' . $type . ': feed age is updated.');
    }
    return TRUE;
  } else {
    if (MOVINO_DEBUG) {
      drupal_set_message('Unknown error - Movino server ' . $server_id . ': feed age not updated.', 'error');
    }
    return FALSE;
  }
}


/**
 * Delete a Movino server.
 *
 * @param integer $id The id of the server.
 * @return boolean
 */
function movino_server_delete($id) {

  $server = movino_server_get($id);
  if ($server != FALSE) {
    $result = db_query("DELETE FROM {movino_servers} WHERE id = '%s' ", $id);
    drupal_set_message('Movino server ' . $server['name'] . ' (' . $server['id'] . ') is deleted.');
    return TRUE;
  }

  drupal_set_message('Server ' . $id . 'does not exist.', 'error');
  return FALSE;
}


/**
 * Converts a suggestion to a valid server id
 * (containing lowercase alphanumeric characters and '-')
 *
 * @param string $id
 * @return string
 */
function _movino_server_id($id) {
  return strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
}


/**
 * The default settings for a Movino server.
 *
 * @return array
 */
function _movino_server_settings_defaults() {
  return array(
    'id' => '',
    'name' => '',
    'description' => '',
    'host' => '',
    'feed_live' => 1,
    'feed_live_port' => '24536',
    'feed_live_path' => 'live.xml',
    'feed_live_updated' => 0,
    'feed_archive' => 1,
    'feed_archive_port' => '24536',
    'feed_archive_path' => 'archived.xml',
    'feed_archive_updated' => 0,
    'source_port' => '30710',
  );
}
